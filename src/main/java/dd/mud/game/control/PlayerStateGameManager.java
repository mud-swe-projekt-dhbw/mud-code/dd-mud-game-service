package dd.mud.game.control;

import dd.mud.game.control.service.GameCreationService;
import dd.mud.game.control.state.AbstractPlayerGameState;
import dd.mud.game.control.state.ChooseGameState;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Optional;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class PlayerStateGameManager {

    private final AutowireCapableBeanFactory autowireCapableBeanFactory;
    private final GameClusterHolder gameClusterHolder;
    private final GameCreationService gameCreationService;

    @Getter
    private HashMap<Long, AbstractPlayerGameState> playerGameStateHashMap = new HashMap<>();

    public void receiveGameMessage (Long playerId, String message) {
        AbstractPlayerGameState playerGameStateFromMap = this.playerGameStateHashMap.get(playerId);
        Optional<AbstractPlayerGameState> gameState =
                (playerGameStateFromMap == null) ? Optional.empty() : Optional.of(playerGameStateFromMap);
        gameState.ifPresentOrElse(
                abstractPlayerGameState -> abstractPlayerGameState.reactToPlayerInputByState(playerId, message),
                () -> enterNewPlayer(playerId)
        );
    }

    private void enterNewPlayer(Long playerId) {
        ChooseGameState playerGameState = new ChooseGameState(playerId);
        autowireCapableBeanFactory.autowireBean(playerGameState);
        this.playerGameStateHashMap.put(playerId, playerGameState);
        playerGameState.showGamesToPlayer();
        // Updates running games via the object reference
        gameCreationService.initGames(gameClusterHolder.getRunningGames());
    }
}
