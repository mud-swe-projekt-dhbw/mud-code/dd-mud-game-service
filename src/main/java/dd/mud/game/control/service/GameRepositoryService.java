package dd.mud.game.control.service;

import dd.mud.game.entity.GameRepository;
import dd.mud.game.entity.model.GameEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameRepositoryService {

    private final GameRepository gameRepository;

    public Iterable<GameEntity> getAllGames () {
        return gameRepository.findAll();
    }
}
