package dd.mud.game.control.service;

import dd.mud.game.control.GameClusterHolder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameIdGetterService {

    private final GameClusterHolder gameClusterHolder;

    public Long getGameIdFromListPosition (Long listPosition) {
        return this.gameClusterHolder.getRunningGames().get(listPosition.intValue() - 1).getGameId();
    }
}
