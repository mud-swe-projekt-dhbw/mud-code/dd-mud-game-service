package dd.mud.game.control.service;

import dd.mud.game.control.gameobject.GameRoom;
import dd.mud.game.control.gameobject.RunningGame;
import dd.mud.game.control.service.mapper.RoomEntityToGameRoomMapper;
import dd.mud.game.entity.GameRepository;
import dd.mud.game.entity.ItemRepository;
import dd.mud.game.entity.ItemRoomRepository;
import dd.mud.game.entity.RoomRepository;
import dd.mud.game.entity.model.GameEntity;
import dd.mud.game.entity.model.Item;
import dd.mud.game.entity.model.ItemRoom;
import dd.mud.game.entity.model.Room;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameCreationService {

    private final GameRepository gameRepository;
    private final RoomRepository roomRepository;
    private final ItemRepository itemRepository;
    private final ItemRoomRepository itemRoomRepository;
    private final RoomEntityToGameRoomMapper roomEntityToGameRoomMapper;

    /**
     *
     * @param alreadyRunningGames Reference to already existing games
     */
    public void initGames(@NonNull List<RunningGame> alreadyRunningGames) {
        Iterable<GameEntity> gameEntities = gameRepository.findAll();
        gameEntities.forEach(gameEntity -> {
            if (!gameIsAlreadyExisting(alreadyRunningGames, gameEntity)) {
                alreadyRunningGames.add(createGameByGameEntity(gameEntity));
            }
        });
    }

    private RunningGame createGameByGameEntity(GameEntity gameEntity) {
        List<GameRoom> gameRoomList = createGameRoomList(gameEntity.getId());
        return RunningGame.builder()
                .gameId(gameEntity.getId())
                .name(gameEntity.getGameName())
                .owner_id(gameEntity.getUserId())
                .gameRoomList(gameRoomList)
                .userCharacterList(new HashMap<>())
                .build();
    }

    private List<GameRoom> createGameRoomList (Long gameId) {
        Iterable<Room> rooms = roomRepository.findAllByGameId(gameId);
        Iterable<Item> items = itemRepository.findAllByGameId(gameId);
        Iterable<ItemRoom> itemIdRoomIdTuple = itemRoomRepository.findAllByRoomId(gameId);

        List<GameRoom> gameRoomList = new ArrayList<>();
        rooms.forEach(room -> gameRoomList.add(roomEntityToGameRoomMapper.mapRoomEntityToGameRoom(
                room,
                items,
                itemIdRoomIdTuple)));
        return gameRoomList;
    }

    private static boolean gameIsAlreadyExisting (List<RunningGame> alreadyRunningGames, GameEntity newGame) {
        boolean alreadyExisting = false;
        for (RunningGame runningGame : alreadyRunningGames) {
            if (runningGame.getGameId().equals(newGame.getId())) {
                alreadyExisting = true;
                break;
            }
        }
        return alreadyExisting;
    }
}
