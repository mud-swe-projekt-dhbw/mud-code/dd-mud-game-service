package dd.mud.game.control.service;

import dd.mud.game.control.GameClusterHolder;
import dd.mud.game.control.gameobject.GameCharacter;
import dd.mud.game.control.service.mapper.CharacterEntityToGameCharacterMapper;
import dd.mud.game.entity.CharacterRepository;
import dd.mud.game.entity.model.CharacterEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SetUpNewPlayerService {

    private final CharacterRepository characterRepository;
    private final GameClusterHolder gameClusterHolder;
    private final CharacterEntityToGameCharacterMapper characterMapper;

    public void setUpNewPlayerById (Long playerId, Long gameId) {
        Optional<CharacterEntity> characterEntity = characterRepository.findByUserIdAndGameId(playerId, gameId);
        if (characterEntity.isPresent()) {
            GameCharacter newCharacter = characterMapper.mapToGameCharacter(characterEntity.get());
            gameClusterHolder.getRunningGames().forEach(runningGame -> {
                if (runningGame.getGameId().equals(gameId)) {
                    runningGame.getUserCharacterList().put(playerId, newCharacter);
                }
            });
        } else {
            throw new RuntimeException("characterEntity not present");
        }
    }
}
