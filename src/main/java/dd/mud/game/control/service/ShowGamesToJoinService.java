package dd.mud.game.control.service;

import dd.mud.game.entity.model.GameEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ShowGamesToJoinService {

    private static final int COUNTER_STARTER = 1;
    private static final String AFTER_COUNTER = ". ";
    private static final String NEW_LINE = "\n";
    public static final String SUFFIX_STAY_IN_SERVICE = ";sis";
    public static final String START_TEXT = "Wählen sie ein Spiel aus (\"1\", \"2\", ...)\n";

    private final GameRepositoryService gameRepositoryService;
    private final AnswerPlayerService answerPlayerService;

    public void showGamesToJoinByPlayerId (Long playerId) {
        answerPlayerService.sendAnswerToPlayer(playerId,
                createGamesString(gameRepositoryService.getAllGames())
        );
    }

    private static String createGamesString (Iterable<GameEntity> gameEntities) {
        StringBuilder stringBuilder = new StringBuilder();
        int counter = COUNTER_STARTER;
        stringBuilder.append(START_TEXT);
        for (GameEntity gameEntity : gameEntities) {
            stringBuilder.append(counter).append(AFTER_COUNTER).append(gameEntity.getGameName()).append(NEW_LINE);
            counter++;
        }
        stringBuilder.append(SUFFIX_STAY_IN_SERVICE);
        return stringBuilder.toString();
    }
}
