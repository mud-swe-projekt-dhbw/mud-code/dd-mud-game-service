package dd.mud.game.control.service;

import dd.mud.game.control.GameClusterHolder;
import dd.mud.game.control.PlayerStateGameManager;
import dd.mud.game.control.gameobject.RunningGame;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ExitGameService {

    private static final String RETURN_TO_MENU_TEXT = "Kehre zum Hauptmenü zurück.\n";
    private static final String RETURN_TO_MENU_SUFFIX = ";rtm";

    private final GameClusterHolder gameClusterHolder;
    private final PlayerStateGameManager playerStateGameManager;
    private final AnswerPlayerService answerPlayerService;

    public void exitCurrentGameByPlayerId (Long playerId, Long gameId, boolean alreadyInGame) {
        if (alreadyInGame) {
            RunningGame runningGame = ControlGameService.getRunningGameById(gameId, this.gameClusterHolder);
            runningGame.getUserCharacterList().remove(playerId);
        }
        playerStateGameManager.getPlayerGameStateHashMap().remove(playerId);
        answerPlayerService.sendAnswerToPlayer(playerId, RETURN_TO_MENU_TEXT + RETURN_TO_MENU_SUFFIX);
    }
}
