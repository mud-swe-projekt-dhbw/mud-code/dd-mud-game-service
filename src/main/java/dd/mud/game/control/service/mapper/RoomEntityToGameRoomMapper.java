package dd.mud.game.control.service.mapper;

import dd.mud.game.control.gameobject.GameItem;
import dd.mud.game.control.gameobject.GameRoom;
import dd.mud.game.control.gameobject.Position;
import dd.mud.game.entity.model.Item;
import dd.mud.game.entity.model.ItemRoom;
import dd.mud.game.entity.model.Room;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class RoomEntityToGameRoomMapper {

    public GameRoom mapRoomEntityToGameRoom(Room room, Iterable<Item> items, Iterable<ItemRoom> itemIdRoomIdTuple) {
        return GameRoom.builder()
                .roomId(room.getId())
                .roomName(room.getRoomName())
                .position(new Position(room.getCoordx(), room.getCoordy()))
                .description(room.getDescrip())
                .itemsInRoomList(getItemListForRoom(room.getId(), createHashMapWithGameItems(items), itemIdRoomIdTuple))
                .build();
    }

    private List<GameItem> getItemListForRoom (Long roomId,
                                               Map<Long, GameItem> itemMap,
                                               Iterable<ItemRoom> itemIdRoomIdTuple) {
        List<Long> roomItemIdsList = new ArrayList<>();
        itemIdRoomIdTuple.forEach(tuple -> {
            if (tuple.getRoomId().equals(roomId)) roomItemIdsList.add(tuple.getItemId());
        });
        List<GameItem> listOfItemsInRoom = new ArrayList<>();
        roomItemIdsList.forEach(id -> listOfItemsInRoom.add(itemMap.get(id)));
        return listOfItemsInRoom;
    }

    private static Map<Long, GameItem> createHashMapWithGameItems (Iterable<Item> gameItemList) {
        HashMap<Long, GameItem> longGameItemHashMap = new HashMap<>();
        gameItemList.forEach(gameItem ->
                longGameItemHashMap.put(gameItem.getId(), mapItemEntityToGameItem(gameItem)));
        return longGameItemHashMap;
    }

    private static GameItem mapItemEntityToGameItem (Item itemEntity) {
        return GameItem.builder()
                .itemId(itemEntity.getId())
                .itemType(GameItem.ItemType.WEAPON)
                .itemName(itemEntity.getItemName())
                .itemValue(itemEntity.getItemValue())
                .build();
    }
}
