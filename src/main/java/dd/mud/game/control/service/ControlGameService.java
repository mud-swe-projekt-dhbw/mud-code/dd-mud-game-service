package dd.mud.game.control.service;

import dd.mud.game.control.GameClusterHolder;
import dd.mud.game.control.gameobject.GameCharacter;
import dd.mud.game.control.gameobject.GameRoom;
import dd.mud.game.control.gameobject.Position;
import dd.mud.game.control.gameobject.RunningGame;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ControlGameService {

    private final GameClusterHolder gameClusterHolder;

    private static final String SIS_SUFFIX = ";sis";

    // In the prototype only simple movements should be possible
    public String processMessage (Long playerId, Long gameId, String message) {
        RunningGame playerGame = getRunningGameById(gameId, this.gameClusterHolder);
        GameCharacter gameCharacter = playerGame.getUserCharacterList().get(playerId);
        Position currentPosition = gameCharacter.getPosition();
        switch (message) {
            case "N":
                currentPosition.setPosY(currentPosition.getPosY() + 1);
                gameCharacter.setPosition(currentPosition);
                break;
            case "O":
                currentPosition.setPosX(currentPosition.getPosX() + 1);
                gameCharacter.setPosition(currentPosition);
                break;
            case "S":
                currentPosition.setPosY(currentPosition.getPosY() - 1);
                gameCharacter.setPosition(currentPosition);
                break;
            case "W":
                currentPosition.setPosX(currentPosition.getPosX() - 1);
                gameCharacter.setPosition(currentPosition);
                break;
            default:
                return "Diese Aktion ist nicht möglich.";
        }
        return getDescriptionFromCurrentRoom(playerId, gameId);
    }

    public String getDescriptionFromCurrentRoom (Long playerId, Long gameId) {
        RunningGame playerGame = getRunningGameById(gameId, this.gameClusterHolder);
        String roomDesc = "Du bist in einem Raum";
        for (GameRoom gameRoom : playerGame.getGameRoomList()) {
            roomDesc = (positionsAreEqual(gameRoom.getPosition(), playerGame.getUserCharacterList().get(playerId).getPosition())) ?
                    gameRoom.getDescription() : roomDesc;
        }
        return roomDesc + SIS_SUFFIX;
    }

    protected static RunningGame getRunningGameById (Long gameId, GameClusterHolder gameClusterHolder) {
        for (RunningGame runningGame : gameClusterHolder.getRunningGames()) {
            if (runningGame.getGameId().equals(gameId)) {
                return runningGame;
            }
        }
        throw new RuntimeException("Game not found");
    }

    private boolean positionsAreEqual (Position pos1, Position pos2) {
        return pos1.getPosX() == pos2.getPosX() && pos1.getPosY() == pos2.getPosY();
    }
}
