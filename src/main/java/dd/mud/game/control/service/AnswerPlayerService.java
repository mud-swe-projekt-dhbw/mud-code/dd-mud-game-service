package dd.mud.game.control.service;

import dd.mud.game.kafka.model.MudKafkaMessage;
import dd.mud.game.kafka.producer.AnswerMessageProducer;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class AnswerPlayerService {

    private final AnswerMessageProducer answerMessageProducer;

    public void sendAnswerToPlayer (Long playerId, String message) {
        answerMessageProducer.sendAnswerBackToWebsocketService(MudKafkaMessage.builder()
                .id(playerId)
                .message(message)
                .build()
        );
    }

}
