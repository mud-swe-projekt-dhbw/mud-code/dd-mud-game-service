package dd.mud.game.control.service.mapper;

import dd.mud.game.control.gameobject.GameCharacter;
import dd.mud.game.control.gameobject.Position;
import dd.mud.game.entity.ClassRepository;
import dd.mud.game.entity.RaceRepository;
import dd.mud.game.entity.model.CharacterEntity;
import dd.mud.game.entity.model.GameClass;
import dd.mud.game.entity.model.Race;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CharacterEntityToGameCharacterMapper {

    private final ClassRepository classRepository;
    private final RaceRepository raceRepository;

    public GameCharacter mapToGameCharacter (CharacterEntity characterEntity) {
        return GameCharacter.builder()
                .characterId(characterEntity.getId())
                .characterName(characterEntity.getChName())
                .charClass(findClassName(characterEntity.getClassId(), characterEntity.getGameId()))
                .race(findRaceName(characterEntity.getRaceId(), characterEntity.getGameId()))
                .userId(characterEntity.getUserId())
                .position(Position.builder().posX(0).posY(0).build())
                .characterGameItems(new ArrayList<>())
                .description(characterEntity.getDescription())
                .personality(characterEntity.getPersonality())
                .build();
    }

    private String findClassName (Long classId, Long gameId) {
        Iterable<GameClass> gameClasses = this.classRepository.findAllByGameId(gameId);
        String returnString = "";
        for (GameClass gameClass : gameClasses) {
            if (gameClass.getId().equals(classId)) returnString = gameClass.getClassName();
        }
        return returnString;
    }

    private String findRaceName (Long raceId, Long gameId) {
        Iterable<Race> gameClasses = this.raceRepository.findAllByGameId(gameId);
        String returnString = "";
        for (Race race : gameClasses) {
            if (race.getId().equals(raceId)) returnString = race.getRaceName();
        }
        return returnString;
    }
}
