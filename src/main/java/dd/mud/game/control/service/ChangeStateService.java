package dd.mud.game.control.service;

import dd.mud.game.control.PlayerStateGameManager;
import dd.mud.game.control.state.AbstractPlayerGameState;
import dd.mud.game.control.state.CharacterConfigurationState;
import dd.mud.game.control.state.PlayState;
import dd.mud.game.control.state.PlayingGameState;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ChangeStateService {

    private final PlayerStateGameManager playerStateGameManager;
    private final AutowireCapableBeanFactory autowireCapableBeanFactory;


    public void changeStateByPlayerIdAndNewState (Long playerId, PlayState newState) {
        switch (newState) {
            case PLAYING:
                PlayingGameState newStateInstance = new PlayingGameState(playerId,
                        playerStateGameManager.getPlayerGameStateHashMap().get(playerId).getJoinedGame());
                this.autowireCapableBeanFactory.autowireBean(newStateInstance);
                playerStateGameManager.getPlayerGameStateHashMap().replace(playerId, newStateInstance);
                newStateInstance.setUpNewPlayer(playerId);
                break;
            case CHARACTER_CONFIGURATION:
                CharacterConfigurationState newStateInstanceConfig = new CharacterConfigurationState(playerId,
                        playerStateGameManager.getPlayerGameStateHashMap().get(playerId).getJoinedGame());
                this.autowireCapableBeanFactory.autowireBean(newStateInstanceConfig);
                playerStateGameManager.getPlayerGameStateHashMap().replace(playerId, newStateInstanceConfig);
                newStateInstanceConfig.sendInitialMessage(playerId);
                break;
            default:
                // do nothing cannot happen
        }
    }
}
