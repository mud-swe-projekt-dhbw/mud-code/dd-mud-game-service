package dd.mud.game.control.gameobject;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
@Builder
public class GameRoom {

    Long roomId;
    String roomName;
    Position position;
    String description;
    List<GameItem> itemsInRoomList;
}
