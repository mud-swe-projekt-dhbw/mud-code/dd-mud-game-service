package dd.mud.game.control.gameobject;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Data
@Builder
public class GameItem {

    Long itemId;
    ItemType itemType;
    String itemName;
    Integer itemValue;


    public enum ItemType {
        WEAPON, POTION
    }
}
