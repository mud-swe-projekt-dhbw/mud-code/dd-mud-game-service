package dd.mud.game.control.gameobject;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
@Builder
public class GameCharacter {

    Long characterId;
    String characterName;
    String charClass;
    String race;
    Long userId;
    String description;
    String personality;

    // Room position
    Position position;

    List<GameItem> characterGameItems;
}
