package dd.mud.game.control.gameobject;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Data
@Builder
public class RunningGame {

    Long gameId;
    String name;
    // Id from the dungeon master that created the game
    Long owner_id;

    List<GameRoom> gameRoomList;
    Map<Long, GameCharacter> userCharacterList;
}
