package dd.mud.game.control.gameobject;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Builder
@Data
public class Position {

    int posX;
    int posY;
}
