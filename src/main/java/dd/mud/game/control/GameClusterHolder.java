package dd.mud.game.control;

import dd.mud.game.control.gameobject.RunningGame;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@AllArgsConstructor
@Builder
@Data
@Component
public class GameClusterHolder {

    List<RunningGame> runningGames;

}
