package dd.mud.game.control.state;

public enum PlayState {
    CHOOSE_GAME,
    CHARACTER_CONFIGURATION,
    PLAYING
}
