package dd.mud.game.control.state;

import dd.mud.game.control.service.ControlGameService;
import dd.mud.game.control.service.SetUpNewPlayerService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

public class PlayingGameState extends AbstractPlayerGameState {

    private static final String SIS_SUFFIX = ";sis";

    @Getter
    private final Long gameId;

    private SetUpNewPlayerService setUpNewPlayerService;
    private ControlGameService controlGameService;

    public PlayingGameState(Long playerId, Long gameId) {
        super(playerId);
        this.gameId = gameId;
    }

    @Override
    protected void reactToPlayerInput(Long playerId, String message) {
        this.answerPlayerService.sendAnswerToPlayer(playerId, this.controlGameService.processMessage(
                playerId, gameId, message
        ));
    }

    @Override
    protected void exitFromCurrentGame(Long playerId) {
        this.exitGameService.exitCurrentGameByPlayerId(playerId, null, true);
    }

    public void setUpNewPlayer (Long playerId) {
        this.setUpNewPlayerService.setUpNewPlayerById(playerId, this.gameId);
        sendInitialMessage(playerId);
        this.answerPlayerService.sendAnswerToPlayer(playerId,
                this.controlGameService.getDescriptionFromCurrentRoom(playerId, this.gameId));
    }

    public void sendInitialMessage (Long playerId) {
        answerPlayerService.sendAnswerToPlayer(playerId, "Dein Abenteuer beginnt...\n" + SIS_SUFFIX);
    }

    @Autowired
    public void setSetUpNewPlayerService (SetUpNewPlayerService setUpNewPlayerService) {
        this.setUpNewPlayerService = setUpNewPlayerService;
    }

    @Autowired
    public void setControlGameService (ControlGameService controlGameService) {
        this.controlGameService = controlGameService;
    }
}
