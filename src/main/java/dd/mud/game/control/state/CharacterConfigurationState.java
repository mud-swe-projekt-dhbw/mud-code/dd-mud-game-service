package dd.mud.game.control.state;

import dd.mud.game.kafka.producer.CharacterConfigurationMessageProducer;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

public class CharacterConfigurationState extends AbstractPlayerGameState {

    @Getter
    private final Long gameId;

    private CharacterConfigurationMessageProducer characterConfigurationMessageProducer;

    public CharacterConfigurationState(Long playerId, Long gameId) {
        super(playerId);
        this.gameId = gameId;
        this.joinedGame = gameId;
    }

    @Override
    public void reactToPlayerInput(Long playerId, String message) {
        characterConfigurationMessageProducer.sendMessage(playerId, message, this.gameId);
    }

    @Override
    protected void exitFromCurrentGame(Long playerId) {
        this.exitGameService.exitCurrentGameByPlayerId(playerId, null, false);
    }

    public void sendInitialMessage (Long playerId) {
        characterConfigurationMessageProducer.sendMessage(playerId, "", this.gameId);
    }

    @Autowired
    public void setCharacterConfigurationMessageProducer (CharacterConfigurationMessageProducer characterConfigurationMessageProducer) {
        this.characterConfigurationMessageProducer = characterConfigurationMessageProducer;
    }
}
