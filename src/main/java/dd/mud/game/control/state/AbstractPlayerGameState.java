package dd.mud.game.control.state;

import dd.mud.game.control.service.AnswerPlayerService;
import dd.mud.game.control.service.ChangeStateService;
import dd.mud.game.control.service.ExitGameService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractPlayerGameState {

    private static final String EXIT_MESSAGE = "exit";

    protected AnswerPlayerService answerPlayerService;
    protected ExitGameService exitGameService;
    protected ChangeStateService changeStateService;

    @Getter
    protected Long playerId;

    @Getter
    protected Long joinedGame = -1L;

    public AbstractPlayerGameState (Long playerId) {
        this.playerId = playerId;
    }

    // Central Method
    public void reactToPlayerInputByState(Long playerId, String message) {
        if (EXIT_MESSAGE.equals(message)) {
            exitFromCurrentGame(playerId);
        } else {
            reactToPlayerInput(playerId, message);
        }
    }

    protected abstract void reactToPlayerInput (Long playerId, String message);

    protected abstract void exitFromCurrentGame (Long playerId);

    @Autowired
    public void setExitGameService (ExitGameService exitGameService) {
        this.exitGameService = exitGameService;
    }

    @Autowired
    public void setAnswerPlayerService (AnswerPlayerService answerPlayerService) {
        this.answerPlayerService = answerPlayerService;
    }

    @Autowired
    public void setChangeStateService (ChangeStateService changeStateService) {
        this.changeStateService = changeStateService;
    }
}
