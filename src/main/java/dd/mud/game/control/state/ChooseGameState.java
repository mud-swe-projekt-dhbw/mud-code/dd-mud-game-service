package dd.mud.game.control.state;

import dd.mud.game.control.service.GameIdGetterService;
import dd.mud.game.control.service.ShowGamesToJoinService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

public class ChooseGameState extends AbstractPlayerGameState {

    private static final String MESSAGE_PATTERN = "[1-9]+";
    private static final String FAILED_MESSAGE = "Ungültige Eingabe. Bitte noch einmal versuchen.;sis";
    private static final String JOIN_GAME_MESSAGE = "Spiel wird beigetreten. Starte Charakterkonfiguration...;sis";

    private ShowGamesToJoinService showGamesToJoinService;
    private GameIdGetterService gameIdGetterService;

    public ChooseGameState (Long playerId) {
        super(playerId);
    }

    @Override
    protected void reactToPlayerInput(Long playerId, String message) {
        if (message.matches(MESSAGE_PATTERN)) {
            this.joinedGame = this.gameIdGetterService.getGameIdFromListPosition(Long.parseLong(message));
            answerPlayerService.sendAnswerToPlayer(playerId, JOIN_GAME_MESSAGE);
            this.changeStateService.changeStateByPlayerIdAndNewState(playerId, PlayState.CHARACTER_CONFIGURATION);
        } else {
            answerPlayerService.sendAnswerToPlayer(playerId, FAILED_MESSAGE);
        }
    }

    @Override
    protected void exitFromCurrentGame(Long playerId) {
        this.exitGameService.exitCurrentGameByPlayerId(playerId, null, false);
    }

    public void showGamesToPlayer () {
        this.showGamesToJoinService.showGamesToJoinByPlayerId(this.getPlayerId());
    }

    @Autowired
    public void setShowGamesToJoinService (ShowGamesToJoinService showGamesToJoinService) {
        this.showGamesToJoinService = showGamesToJoinService;
    }

    @Autowired
    public void setGameIdGetterService (GameIdGetterService gameIdGetterService) {
        this.gameIdGetterService = gameIdGetterService;
    }
}
