package dd.mud.game.entity;

import dd.mud.game.entity.model.GameClass;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassRepository extends CrudRepository<GameClass, Long> {

    boolean existsByClassName(String classname);
    Iterable<GameClass> findAllByGameId (Long gameId);
}
