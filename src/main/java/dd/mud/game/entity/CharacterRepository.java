package dd.mud.game.entity;

import dd.mud.game.entity.model.CharacterEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CharacterRepository extends CrudRepository<CharacterEntity, Long> {
    Optional<CharacterEntity> findByUserIdAndGameId (Long userId, Long gameId);
}
