package dd.mud.game.entity;

import dd.mud.game.entity.model.Room;
import org.springframework.data.repository.CrudRepository;

public interface RoomRepository extends CrudRepository<Room, Long> {
    Iterable<Room> findAllByGameId (Long gameId);
}
