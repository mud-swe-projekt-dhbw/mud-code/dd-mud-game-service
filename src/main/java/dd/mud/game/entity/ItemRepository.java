package dd.mud.game.entity;

import dd.mud.game.entity.model.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {

    boolean existsByItemName(String itemName);
    Iterable<Item> findAllByGameId (Long gameId);
}
