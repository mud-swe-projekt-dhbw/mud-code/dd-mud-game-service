package dd.mud.game.entity;

import dd.mud.game.entity.model.ItemRoom;
import dd.mud.game.entity.model.ItemRoomId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRoomRepository extends CrudRepository<ItemRoom, Long> {

    boolean existsByRoomIdAndItemId(Long roomId, Long itemId);
    Iterable<ItemRoom> findAllByRoomId (Long roomId);
}
