package dd.mud.game.entity;

import dd.mud.game.entity.model.Race;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RaceRepository extends CrudRepository<Race, Long> {

    boolean existsByRaceName(String raceName);
    Iterable<Race> findAllByGameId (Long gameId);
}
