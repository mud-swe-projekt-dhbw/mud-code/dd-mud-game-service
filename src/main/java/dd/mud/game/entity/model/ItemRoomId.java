package dd.mud.game.entity.model;

import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public class ItemRoomId implements Serializable {

    private Long roomId;

    private Long itemId;
}
