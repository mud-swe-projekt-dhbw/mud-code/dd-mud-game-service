package dd.mud.game;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DdMudGameServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DdMudGameServiceApplication.class, args);
	}

}
