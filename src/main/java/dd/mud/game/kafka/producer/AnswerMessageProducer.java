package dd.mud.game.kafka.producer;

import dd.mud.game.kafka.model.MudKafkaMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class AnswerMessageProducer {

    private static final String SEND_BACK_TOPIC = "playAnswer.t";

    private final KafkaTemplate<String, MudKafkaMessage> kafkaMessageKafkaTemplate;

    public void sendAnswerBackToWebsocketService (MudKafkaMessage mudKafkaMessage) {
        System.out.println("ansering player " + mudKafkaMessage.getId() + ": " + mudKafkaMessage.getMessage());
        kafkaMessageKafkaTemplate.send(SEND_BACK_TOPIC, mudKafkaMessage);
    }
}
