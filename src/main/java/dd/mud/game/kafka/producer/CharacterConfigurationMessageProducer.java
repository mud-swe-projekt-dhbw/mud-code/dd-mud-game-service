package dd.mud.game.kafka.producer;

import dd.mud.game.kafka.model.CharacterConfigurationKafkaMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CharacterConfigurationMessageProducer {

    private static final String SEND_TO_TOPIC = "characterConfigSend.t";

    private final KafkaTemplate<String, CharacterConfigurationKafkaMessage> kafkaMessageKafkaTemplate;

    public void sendMessage (Long playerId, String message, Long gameId) {
        System.out.println("outgoing to char config: " + message);
        this.kafkaMessageKafkaTemplate.send(SEND_TO_TOPIC, CharacterConfigurationKafkaMessage.builder()
                .id(playerId)
                .message(message)
                .gameId(gameId)
                .build()
        );
    }
}
