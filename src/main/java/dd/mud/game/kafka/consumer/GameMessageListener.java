package dd.mud.game.kafka.consumer;

import dd.mud.game.control.PlayerStateGameManager;
import dd.mud.game.kafka.model.MudKafkaMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class GameMessageListener {

    private final PlayerStateGameManager playerStateGameManager;

    @KafkaListener(topics = "playSend.t", groupId = "gameConsumer")
    public void consumeGameMessage (MudKafkaMessage mudKafkaMessage) {
        System.out.println("incoming message from player: " + mudKafkaMessage.getId() + " " + mudKafkaMessage.getMessage());
        playerStateGameManager.receiveGameMessage(mudKafkaMessage.getId(), mudKafkaMessage.getMessage());
    }
}
