package dd.mud.game.kafka.consumer;

import dd.mud.game.control.service.ChangeStateService;
import dd.mud.game.control.state.PlayState;
import dd.mud.game.kafka.model.MudKafkaMessage;
import dd.mud.game.kafka.producer.AnswerMessageProducer;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CharacterConfigurationAnswerListener {

    private static final String GO_TO_PLAYING_SUFFIX = ";sp";
    private static final String STAY_IN_STATE_SUFFIX = ";sis";

    private final AnswerMessageProducer answerMessageProducer;
    private final ChangeStateService changeStateService;

    @KafkaListener(topics = "characterConfigAnswer.t", groupId = "charConfigConsumer")
    public void listenToCharacterConfigAnswers (MudKafkaMessage message) {
        System.out.println("incoming from char config: " + message);
        if (message.getMessage().endsWith(GO_TO_PLAYING_SUFFIX)) {
            changeStateService.changeStateByPlayerIdAndNewState(message.getId(), PlayState.PLAYING);
            String alteredMessage = message.getMessage().substring(0, message.getMessage().length()-3) + STAY_IN_STATE_SUFFIX;
            answerMessageProducer.sendAnswerBackToWebsocketService(MudKafkaMessage.builder()
                    .id(message.getId())
                    .message(alteredMessage)
                    .build()
            );
        } else {
            answerMessageProducer.sendAnswerBackToWebsocketService(message);
        }
    }
}
